﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

static class AnimFrames
{
    public static string DEMON_HEAD_1 = @"|    ■ ■ ■ ■    |      
                                   |  ■         ■  |      
                                   | ■   <>  <>   ■|       
                                   | ■           ■ |       
                                   |  ■     ■   ■  |       
                                   |  ■    ■    ■  |       
                                   |  ■ ■ ■    ■   |       
                                   | ■       ■     |       
                                   | ■ ■ ■ ■       |";

    public static string DEMON_HEAD_3 = @"  XXXXXXXXXXXXXX
                                      XXXXXXX          XXXXXXX
                                   XXXXXX                   XXXXX
                                   XXX                         XXXX
                                  XX                             XXX
                                 XX                                X
                                X                                  XX
                                X                                   X
                                X                                   XX
                               X                                   XXXX
                               X                                 XXX  X
                               XX                           XXXXXX    X
                                X                         XX          X
                              XX                          X           X
                              X   XXXX             XXX    X          X
                              XXXX    XX         XXX XXXXXX          X
                               X      XXX      XXX       XX         XX
                               X        XX     X          XX       XX
                              XXX       XX     XX         XXXX   XXX
                              X XXXXXXXXX        XXXXX  XXX  XXXXX
                              X           XXXX       XXX    XXXX
                              XXXXX      XX   XX          XXX
                                  XXX    X    XX    XXX/XXX XX
                                  XXX    XXXXXX     X///X   X
                                   XX               X///X   X
                                   XXXX    XXXXXXXXX //XX   XX
                                   XXXXXXXXX/////////XXX  XXXX
                                    XXX////////XXXXXX    XX  XX
                                     XXXXXXXXXXX      XXXX    X
                                      XX            XXX       XX
                                       XXX        XX           XX
                                         XXXXXXXXX              XX
                                           XX                    XX
                                            X                     XXX
                                            X                     X XXXXXX X
                                            XXX                             X XXXXX XX
                                XXXX XXXXXXXXX                                        XXXX
                          XXXXXXXXXXXXXX
                         XXXXXX X
                         XXX                                                                ";

    public static string DEMON_HEAD_4 = @"XXXXXXXXXXXXXX
                                     XXXXXXX          XXXXXXX
                                  XXXXXX                   XXXXX
                                  XXX                         XXXX
                                 XX                             XXX
                                XX                                X
                               X                                  XX
                               X                                   X
                               X                                   XX
                              X                                   XXXX
                              X                                 XXX  X
                              XX                           XXXXXX    X
                               X                         XX          X
                             XX                          X           X
                             X   XXXX             XXX    X          X
                             XXXX    XX         XXX XXXXXX          X
                              X      XXX      XXX       XX         XX
                              X        XX     X          XX       XX
                             XXX       XX     XX         XXXX   XXX
                             X XXXXXXXXX        XXXXX  XXX  XXXXX
                             X           XXXX       XXX    XXXX
                             XXXXX      XX   XX          XXX
                                 XXX    X    XX    XXX/XXXXXX
                                 XXX    XXXXXX     X/\\  X X
                                  XX               X\\\  X X
                                 XXXXX    XXXXXXXX\\\\\ \XXXX
                                  XXXXXXXXX/\\\\\\\\\\\\\XXXX
                                   X\\\\\\\\\\       \\\\XX XX
                                   XXXX            \\\\XXX   X
                                    XXX\\\\\\\\\\\\\\\XXX    XX
                                    XXXXXXXXXXXXXXXXXX  X     XX
                                      XXX             XX       XX
                                        XXXXXXXXXXXXXXX         XX
                                           X                     XXX
                                           X                     X XXXXXX X
                                           XXX                             X XXXXX XX
                               XXXX XXXXXXXXX                                        XXXX
                         XXXXXXXXXXXXXX
                        XXXXXX X
                        XXX                                                                ";


}
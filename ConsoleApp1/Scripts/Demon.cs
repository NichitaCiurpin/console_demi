﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

static class Demon
{
    static Timer _timer1 = new Timer(300);
    static int frame = 1;
    static bool isDrawFrame = true;

    static Demon()
    {
        _timer1.Elapsed += SetAnimFrame;
        _timer1.Start();
    }

    static public void SetAnimFrame(Object source, ElapsedEventArgs e)
    {
        isDrawFrame = true;
        if (frame == 1)
            frame = 2;
        else if (frame == 2)
            frame = 1;
    }

    static public void DrawSelf()
    {
        if (isDrawFrame)
        {
            isDrawFrame = false;
            if (frame == 1)
            {
                Console.Clear();
                Console.WriteLine("                 ■ ■ ■ ■               ");
                Console.WriteLine("               ■         ■             ");
                Console.WriteLine("             ■   <>  <>   ■            ");
                Console.WriteLine("             ■           ■             ");
                Console.WriteLine("              ■     ■   ■              ");
                Console.WriteLine("              ■    ■    ■              ");
                Console.WriteLine("              ■ ■ ■    ■               ");
                Console.WriteLine("             ■       ■                 ");
                Console.WriteLine("             ■ ■ ■ ■                   ");
            }
            else if (frame == 2)
            {
                Console.Clear();
                Console.WriteLine("                ■ ■ ■ ■                ");
                Console.WriteLine("              ■         ■              ");
                Console.WriteLine("            ■  <>   <>   ■             ");
                Console.WriteLine("           ■           ■               ");
                Console.WriteLine("            ■      ■ ■                 ");
                Console.WriteLine("             ■     ■   ■               ");
                Console.WriteLine("             ■    ■   ■                ");
                Console.WriteLine("            ■ ■ ■   ■                  ");
                Console.WriteLine("            ■ ■ ■ ■                    ");
            }
        }
    }
}

